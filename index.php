<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Vancity hotels</title>
        <meta itemprop="name" content="Vancity Hotels">
        <meta itemprop="description" content="Best Hotels in Vancouver BC Canada">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <span style="color:navajowhite"><strong>Hotels Inn Vancity</strong></span><hr>
        <!--
        <div style="padding-left:30px;text-align:left;">
        <select>
            <option value="Best">Best</option>
        </select>
        </div>-->
        <br>
        <img src="img/vancouvermap.gif" width="70%"/>

        <!-- Images -->
        <div id="hotel_fpr">
            <img id="img_a_fpr" src="img/arch_fairmont_pacific_rim.png" />
        </div>

        <div id="hotel_sl">
            <img id="img_a_sl" src="img/arch_shangri_la.jpg"/>
        </div>

        <div id="hotel_rh">
            <img id="img_a_rh" src="img/arch_rosewood_hotel.jpg"/>
        </div>

        <div id="hotel_fs">
            <img id="img_a_fs" src="img/arch_four_seasons.jpg" />
        </div>

        <div id="hotel_lh">
            <img id="img_a_lh" src="img/arch_hermitage.jpg" />
        </div>

        <div id="hotel_l">
            <img id="img_a_l" src="img/arch_loden.jpg" />
        </div>

        <!-- Content -->
        <?php
        $string = file_get_contents("data/hotels.json");
        $json_a = json_decode($string, true);
        foreach ($json_a as $person_name => $h) {

            echo '<div id="ctn_hotel_' . $h['id'] .'" style="display: none">';
                echo $h['name'] . '<br>';
                echo 'Phone: ' . $h['phone'] . '<br>';
                echo 'Address: ' . $h['address'] . '<br>';
                echo '<br>';
                echo 'Estimated price: ' . $h['price'] . '<br>';
                echo 'Photos: ' . '<br>';
                echo '<span id="p_lt"><</span>';
                echo '--PHOTO--';
                echo '<span id="p_rt">></span>';
                echo '<br><br>';
                if ($h['shopping']!='') {
                    echo "<strong>To Do</strong><br>";
                    echo "<a href='".$h['shopping'][0]['link']."'>";
                        echo "<img src='img/".$h['shopping'][0]['img']."' width=30 height=30 /> ";
                        echo($h['shopping'][0]['name']);
                    echo "</a>";
                };
                echo '<br><br>';


            echo '</div>';
        }

        ?>

        <br><br>
        The best luxury hotels in Vancouver Canada.<br>
        From about $358 a night<br>

        <br><br><br>
        <div>
            © Copyright by Vancity Hotels<br>
            <span style="font-size:0.5em">Photo attributes to <a href="https://www.flickr.com/photos/sagamiono/">Micheal</a>, Klazu, <a href="https://www.flickr.com/photos/urbanmixer/">Raj Taneja</a>, <a href="https://www.flickr.com/photos/bouldair/">Andrew Hyde</a>, <a href="https://commons.wikimedia.org/wiki/File:Waves_at_Shangri-la.JPG">Chloeq521</a>, <a href="https://www.flickr.com/photos/o_0/">Guilhem Vellut</a>, <a href="https://www.flickr.com/photos/keepitsurreal/">Kyle Pearce</a><a href="https://www.flickr.com/photos/cfccreates/">Canadian Film Centre</a>, <a href="https://www.flickr.com/photos/ikkoskinen/">IK's World Trip</a>, <a href="https://www.flickr.com/photos/wicker-furniture/">Wicker Paradise</a>, <a href="https://commons.wikimedia.org/wiki/User:Xicotencatl">Xicotencatl</a>. Images may have been modified and cut to fit design.</span>

        </div>

        <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
        <script>
            $(function() {
                $('*').click(function (e) {

                    if (e.target.id == 'img_a_fpr') {
                        $('#ctn_hotel_sl').hide();
                        $('#ctn_hotel_l').hide();
                        $('#ctn_hotel_lh').hide();
                        $('#ctn_hotel_fs').hide();
                        $('#ctn_hotel_rh').hide();
                        $("#ctn_hotel_fpr").show("slow");
                    }

                    else if (e.target.id == 'img_a_l') {
                        $('#ctn_hotel_fpr').hide();
                        $('#ctn_hotel_sl').hide();
                        $('#ctn_hotel_lh').hide();
                        $('#ctn_hotel_fs').hide();
                        $('#ctn_hotel_rh').hide();
                        $("#ctn_hotel_l").show("slow");
                    }

                    else if (e.target.id == 'img_a_lh') {
                        $('#ctn_hotel_fpr').hide();
                        $('#ctn_hotel_sl').hide();
                        $('#ctn_hotel_l').hide();
                        $('#ctn_hotel_fs').hide();
                        $('#ctn_hotel_rh').hide();
                        $("#ctn_hotel_lh").show("slow");
                    }

                    else if (e.target.id == 'img_a_fs') {
                        $('#ctn_hotel_fpr').hide();
                        $('#ctn_hotel_sl').hide();
                        $('#ctn_hotel_l').hide();
                        $('#ctn_hotel_lh').hide();
                        $('#ctn_hotel_rh').hide();
                        $("#ctn_hotel_fs").show("slow");
                    }

                    else if (e.target.id == 'img_a_rh') {
                        $('#ctn_hotel_fpr').hide();
                        $("#ctn_hotel_rh").show("slow");
                    }

                    else if (e.target.id == 'img_a_sl') {
                        $('#ctn_hotel_fpr').hide();
                        $('#ctn_hotel_l').hide();
                        $('#ctn_hotel_lh').hide();
                        $('#ctn_hotel_fs').hide();
                        $('#ctn_hotel_rh').hide();
                        $("#ctn_hotel_sl").show("slow");
                    }

                    else if (e.target.id.substring(0, 6) != "ctn_ho" && e.target.id != 'p_lt' && e.target.id != 'p_rt') {
                        $('#ctn_hotel_fpr').hide();
                        $('#ctn_hotel_sl').hide();
                        $('#ctn_hotel_l').hide();
                        $('#ctn_hotel_lh').hide();
                        $('#ctn_hotel_fs').hide();
                        $('#ctn_hotel_rh').hide();
                    }
                });
            });
        </script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
